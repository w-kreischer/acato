var team = document.querySelector('#team-grid');
var requestURL = '/json/team.json';
var request = new XMLHttpRequest();

request.open('GET', requestURL);
request.responseType = 'json';
request.send();
request.onload = function() {
    var members = request.response;
    showMembers(members);
    filterSelection("all");
}

// Show members
function showMembers(jsonObj) {
    var members = jsonObj['members'];
    for (var i = 0; i < members.length; i++) {
        var member = document.createElement('li');
        var memberName = document.createElement('h5');
        var memberImage = document.createElement('img');
        var memberFunction = document.createElement('p');
        member.className = 'member ' + (members[i].filter);
        memberName.className = 'member__name';
        memberImage.className = 'member__image';
        memberFunction.className = 'member__function';
        memberName.textContent = members[i].name;
        memberImage.setAttribute("src", "/img/" + members[i].picture + ".jpg");
        memberImage.setAttribute("alt", members[i].name);
        memberFunction.textContent = members[i].function;
        member.appendChild(memberImage);
        member.appendChild(memberName);
        member.appendChild(memberFunction);
        team.appendChild(member);
    }
}

// Filter members
function filterSelection(c) {
    var x, i;
    x = document.getElementsByClassName("member");
    if (c == "all") c = "";
    for (i = 0; i < x.length; i++) {
        removeClass(x[i], "show");
        if (x[i].className.indexOf(c) > -1) addClass(x[i], "show");
    }
}
// Show filtered elements
function addClass(element, name) {
    var i, arr1, arr2;
    arr1 = element.className.split(" ");
    arr2 = name.split(" ");
    for (i = 0; i < arr2.length; i++) {
        if (arr1.indexOf(arr2[i]) == -1) {
            element.className += " " + arr2[i];
        }
    }
}
// Hide filtered elements
function removeClass(element, name) {
    var i, arr1, arr2;
    arr1 = element.className.split(" ");
    arr2 = name.split(" ");
    for (i = 0; i < arr2.length; i++) {
        while (arr1.indexOf(arr2[i]) > -1) {
            arr1.splice(arr1.indexOf(arr2[i]), 1);
        }
    }
    element.className = arr1.join(" ");
}
var btnContainer = document.getElementById("button-container__filter");
var btns = btnContainer.getElementsByClassName("btn");
for (var i = 0; i < btns.length; i++) {
    btns[i].addEventListener("click", function() {
        var current = document.getElementsByClassName("is-active");
        current[0].className = current[0].className.replace("is-active", "");
        this.className += " is-active";
    });
}

var filterAll = document.querySelector(".filter-all");
var filterUserExperience = document.querySelector(".filter-user-experience");
var filterRealisatie = document.querySelector(".filter-realisatie");
var filterOfficeManagement = document.querySelector(".filter-office-management");
var filterKwispelaar = document.querySelector(".filter-kwispelaar");
var filterOntwerpAnimatie = document.querySelector(".filter-ontwerp-en-animatie");
var filterUxStagiaire = document.querySelector(".filter-ux-stagiaire");
var filterRecruitment = document.querySelector(".filter-recruitment");
var filterAccount = document.querySelector(".filter-account");
var filterArtDirector = document.querySelector(".filter-art-director");
var filterOntwerp = document.querySelector(".filter-ontwerp");
var filterUxDirector = document.querySelector(".filter-ux-director");
var filterInteractieontwerp = document.querySelector(".filter-interactieontwerp");
var filterAccount = document.querySelector(".filter-account");
var filterManaging = document.querySelector(".filter-managing");

filterAll.addEventListener("click", function() {
	filterSelection('all');
});
filterAccount.addEventListener("click", function() {
	filterSelection('filter-account');
});
filterManaging.addEventListener("click", function() {
	filterSelection('filter-managing');
});
filterUserExperience.addEventListener("click", function() {
	filterSelection('filter-user-experience');
});
filterRealisatie.addEventListener("click", function() {
	filterSelection('filter-realisatie');
});
filterOfficeManagement.addEventListener("click", function() {
	filterSelection('filter-office-management');
});
filterKwispelaar.addEventListener("click", function() {
	filterSelection('filter-kwispelaar');
});
filterOntwerpAnimatie.addEventListener("click", function() {
	filterSelection('filter-ontwerp-en-animatie');
});
filterUxStagiaire.addEventListener("click", function() {
	filterSelection('filter-ux-stagiaire');
});
filterRecruitment.addEventListener("click", function() {
	filterSelection('filter-recruitment');
});
filterArtDirector.addEventListener("click", function() {
	filterSelection('filter-art-director');
});
filterOntwerp.addEventListener("click", function() {
	filterSelection('filter-ontwerp');
});
filterUxDirector.addEventListener("click", function() {
	filterSelection('filter-ux-director');
});
filterInteractieontwerp.addEventListener("click", function() {
	filterSelection('filter-interactieontwerp');
});

// Form: replace validation UI
function replaceValidationUI(form) {
    form.addEventListener("invalid", function(event) {
        event.preventDefault();
    }, true);
    form.addEventListener("submit", function(event) {
        if (!this.checkValidity()) {
            event.preventDefault();
        }
    });
    form.insertAdjacentHTML("afterbegin", "<ul class='error-messages'></ul>");
    var submitButton = form.querySelector("button:not([type=button]), input[type=submit]");
    submitButton.addEventListener("click", function(event) {
        var invalidFields = form.querySelectorAll(":invalid"),
            listHtml = "",
            errorMessages = form.querySelector(".error-messages"),
            label;
        for (var i = 0; i < invalidFields.length; i++) {
            label = form.querySelector("label[for=" + invalidFields[i].id + "]");
            listHtml += "<li>" + "<label>" + label.innerHTML + "</label>" + invalidFields[i].validationMessage + "</li>";
        }
        errorMessages.innerHTML = listHtml;
        if (invalidFields.length > 0) {
            invalidFields[0].focus();
            errorMessages.style.display = "block";
        }
    });
}
var forms = document.querySelectorAll("form");
for (var i = 0; i < forms.length; i++) {
    replaceValidationUI(forms[i]);
}